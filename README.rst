.. contents:: Table of Contents

========
Overview
========

The Supply Chain Manager is invoked by the SLO Manager in the SLA negotiation / renegotiation phase to build a set of valid supply chains on top of which the SLO Manager creates SLA Offers which are returned to the SPECS Application.

The Supply Chain Manager first extracts security metrics from the SLOs specified by the End-user. For each specified metric, security mechanisms are retrieved by the Supply Chain Manager from the Service Manager. Given all these mechanisms, the Supply Chain Manager identifies all different subsets of them that are able to fulfil End-user’s requests.

For each of these sets of mechanisms, the Supply Chain Manager launches a process of supply chain building on the Planning component. The Planning component utilizes an analytical solver based on linear programming optimization, which solves the so-called planning problem and finally returns a supply chain. This process is iterated over all possible resource combinations and over all possible mechanisms sets, and results in a set of valid (i.e., feasible) supply chains that are returned to the SLO Manager.

The process of supply chain building is illustrated in the figure below:

|

.. image:: https://bitbucket.org/specs-team/specs-core-negotiation-supply_chain_manager/raw/master/docs/images/building_supply_chains.png

**Figure: Supply chain building during negotiation**

============
Installation
============

Prerequisites:

- Java 7

The Supply Chain Manager is a Java library which can be included in the Maven-based project as a dependency:

.. code-block:: xml

 <dependency>
    <groupId>eu.specs-project.core.negotiation</groupId>
    <artifactId>supply-chain-manager</artifactId>
    <version>0.1-SNAPSHOT</version>
 </dependency>

In non-Maven project the Supply Chain Manager can be downloaded manually from the SPECS Maven repository and included in the project's classpath::

 https://nexus.services.ieat.ro/nexus/content/repositories/specs-snapshots/eu/specs-project/core/negotiation/supply-chain-manager/

To build the Supply Chain Manager component from source code you need the Apache Maven 3 tool. First clone the project from the Bitbucket repository using a Git client::

 git clone https://bitbucket.org/specs-team/specs-core-negotiation-supply_chain_manager

then go into the cloned directory and run::

 mvn package

=====
Usage
=====

The Supply Chain Manager provides following API:

.. code-block:: java

 SupplyChainManager(String serviceManagerApiAddress, String planningApiAddress)

 List<SupplyChain> buildSupplyChains(AgreementOffer agreementOffer) throws SupplyChainManagerException

======
Notice
======

This product includes software developed at "XLAB d.o.o, Slovenia", as part of the "SPECS - Secure Provisioning of Cloud Services based on SLA Management" research project (an EC FP7-ICT Grant, agreement 610795).

- http://www.specs-project.eu/
- http://www.xlab.si/

Developers:

- Jolanda Modic, jolanda.modic@xlab.si
- Damjan Murn, damjan.murn@xlab.si

Copyright:

.. code-block:: 

 Copyright 2013-2015, XLAB d.o.o, Slovenia
    http://www.xlab.si

 Supply Chain Manager is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License, version 3,
 as published by the Free Software Foundation.

 Supply Chain Manager is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.