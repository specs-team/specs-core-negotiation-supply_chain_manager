package eu.specs.project.negotiation.scm.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.datamodel.enforcement.SupplyChainActivity;
import eu.specs.project.negotiation.scm.exceptions.SupplyChainManagerException;
import eu.specs.project.negotiation.scm.utils.JsonDumper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

public class PlanningClient {
    private static final Logger logger = LogManager.getLogger(PlanningClient.class);
    private WebTarget planningApiTarget;

    public PlanningClient(String planningApiAddress) {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        planningApiTarget = client.target(planningApiAddress);
    }

    public SupplyChainActivity createSupplyChainActivity(SupplyChainActivity scaInput) throws SupplyChainManagerException {
        try {
            logger.debug("Sending request to Planning to create supply chain activity for SLA {}...", scaInput.getSlaId());
            SupplyChainActivity sca = planningApiTarget
                    .path("/sc-activities")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(scaInput), SupplyChainActivity.class);

            logger.debug("Supply chain activity has been created successfully.");
            return sca;

        } catch (Exception e) {
            throw new SupplyChainManagerException("Failed to create supply chain activity: " + e.getMessage(), e);
        }

    }

    public SupplyChainActivity.Status getSCAStatus(SupplyChainActivity sca) throws SupplyChainManagerException {
        try {
            logger.debug("Retrieving supply chain activity {} status...", sca.getId());
            Map<String, SupplyChainActivity.Status> statusResponse = planningApiTarget
                    .path("/sc-activities/{scaId}/state")
                    .resolveTemplate("scaId", sca.getId())
                    .request(MediaType.APPLICATION_JSON)
                    .get(new GenericType<Map<String, SupplyChainActivity.Status>>() {
                    });

            logger.debug("SCA status has been retrieved successfully: {}", JsonDumper.dump(statusResponse));
            return statusResponse.get("state");

        } catch (Exception e) {
            throw new SupplyChainManagerException("Failed to retrieve supply chain activity state: " + e.getMessage(), e);
        }
    }

    public List<SupplyChain> getSupplyChains(SupplyChainActivity sca) throws SupplyChainManagerException {
        try {
            List<SupplyChain> supplyChains = planningApiTarget
                    .path("/sc-activities/{scaId}/supply-chains")
                    .resolveTemplate("scaId", sca.getId())
                    .request(MediaType.APPLICATION_JSON)
                    .get(new GenericType<List<SupplyChain>>() {
                    });

            return supplyChains;

        } catch (Exception e) {
            throw new SupplyChainManagerException(
                    "Failed to retrieve built supply chains from the Planning: " + e.getMessage(), e);
        }
    }
}
