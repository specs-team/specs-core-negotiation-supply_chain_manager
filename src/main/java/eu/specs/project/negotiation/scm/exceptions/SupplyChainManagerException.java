package eu.specs.project.negotiation.scm.exceptions;

public class SupplyChainManagerException extends Exception {

    public SupplyChainManagerException() {
        super();
    }

    public SupplyChainManagerException(String message) {
        super(message);
    }

    public SupplyChainManagerException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
