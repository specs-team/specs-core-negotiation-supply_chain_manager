package eu.specs.project.negotiation.scm;

import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.slo.ServiceProperties;
import eu.specs.datamodel.agreement.slo.Variable;
import eu.specs.datamodel.agreement.terms.GuaranteeTerm;
import eu.specs.datamodel.agreement.terms.ServiceDescriptionTerm;
import eu.specs.datamodel.agreement.terms.Term;
import eu.specs.datamodel.enforcement.CloudResources;
import eu.specs.datamodel.enforcement.Slo;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.datamodel.enforcement.SupplyChainActivity;
import eu.specs.datamodel.sla.sdt.SLOType;
import eu.specs.datamodel.sla.sdt.SLOexpressionType;
import eu.specs.datamodel.sla.sdt.ServiceDescriptionType;
import eu.specs.project.negotiation.scm.client.PlanningClient;
import eu.specs.project.negotiation.scm.client.ServiceManagerClient;
import eu.specs.project.negotiation.scm.exceptions.SupplyChainManagerException;
import eu.specs.project.negotiation.scm.utils.CollectionSchema;
import eu.specs.project.negotiation.scm.utils.JsonDumper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SupplyChainManager {
    private static final Logger logger = LogManager.getLogger(SupplyChainManager.class);
    private ServiceManagerClient serviceManagerClient;
    private PlanningClient planningClient;

    public SupplyChainManager(String serviceManagerApiAddress, String planningApiAddress) {
        serviceManagerClient = new ServiceManagerClient(serviceManagerApiAddress);
        planningClient = new PlanningClient(planningApiAddress);
    }

    public List<SupplyChain> buildSupplyChains(AgreementOffer agreementOffer) throws SupplyChainManagerException {
        try {
            return buildSupplyChainsImpl(agreementOffer);
        } catch (Exception e) {
            logger.error("Failed to build supply chains: " + e.getMessage(), e);
            throw new SupplyChainManagerException("Failed to build supply chains: " + e.getMessage(), e);
        }
    }

    private List<SupplyChain> buildSupplyChainsImpl(AgreementOffer agreementOffer)
            throws SupplyChainManagerException {

        logger.debug("Building supply chains for the agreement offer {}.", agreementOffer.getContext().getTemplateName());

        List<CloudResources> cloudResourcesList = new ArrayList<>();

        // extract metrics from the agreement offer and retrieve security mechanisms from the Service Manager
        ServiceDescriptionTerm serviceDescriptionTerm = null;
        for (Term t : agreementOffer.getTerms().getAll().getAll()) {
            if (t instanceof ServiceDescriptionTerm) {
                serviceDescriptionTerm = (ServiceDescriptionTerm) t;
                break;
            }
        }

        if (serviceDescriptionTerm == null) {
            throw new SupplyChainManagerException("No ServiceDescriptionTerm found in the AgreementOffer.");
        }

        ServiceDescriptionType.ServiceResources serviceResources =
                serviceDescriptionTerm.getServiceDescription().getServiceResources().get(0);

        for (ServiceDescriptionType.ServiceResources.ResourcesProvider resourcesProvider : serviceResources.getResourcesProvider()) {
            CloudResources cloudResources = new CloudResources();
            cloudResourcesList.add(cloudResources);
            cloudResources.setProviderId(resourcesProvider.getId());
            CloudResources.Zone zone = new CloudResources.Zone();
            cloudResources.addZone(zone);
            zone.setZoneId(resourcesProvider.getZone());
            zone.setMaxAllowedVMs(resourcesProvider.getMaxAllowedVMs());
            for (ServiceDescriptionType.ServiceResources.ResourcesProvider.VM vm : resourcesProvider.getVM()) {
                CloudResources.VmType vmType = new CloudResources.VmType();
                vmType.setAppliance(vm.getAppliance());
                vmType.setHardware(vm.getHardware());
                zone.addVmType(vmType);
            }
        }

        // metric variables
        Map<String, String> metricVariableMapping = new HashMap<>();
        for (Term term : agreementOffer.getTerms().getAll().getAll()) {
            if (term instanceof ServiceProperties) {
                ServiceProperties serviceProperties = (ServiceProperties) term;
                for (Variable variable : serviceProperties.getVariableSet().getVariables()) {
                    metricVariableMapping.put(variable.getName(), variable.getMetric());
                }
            }
        }

        List<Slo> slos = new ArrayList<>();
        List<String> metrics = new ArrayList<>();

        // iterate over guarantee terms
        for (Term term : agreementOffer.getTerms().getAll().getAll()) {
            if (term instanceof GuaranteeTerm) {
                GuaranteeTerm guaranteeTerm = (GuaranteeTerm) term;

                // extract capability from the guarantee term name
                Pattern termPattern = Pattern.compile("//specs:capability\\[@id='([^']+)'\\]");
                Matcher m = termPattern.matcher(guaranteeTerm.getName());
                String capability;
                if (m.find()) {
                    capability = m.group(1);
                } else {
                    throw new SupplyChainManagerException(
                            "Failed to extract capability from the guarantee term name: " + guaranteeTerm.getName());
                }

                // iterate over SLOs
                for (SLOType sloType : guaranteeTerm.getServiceLevelObjective().getCustomServiceLevel().getObjectiveList().getSLO()) {
                    String metric = metricVariableMapping.get(sloType.getMetricREF());
                    if (metric == null) {
                        throw new SupplyChainManagerException(String.format(
                                "Variable '%s' referenced in SLO '%s' cannot be found.", sloType.getMetricREF(), sloType.getSLOID()));
                    }
                    metrics.add(metric);

                    Slo slo = new Slo();
                    slos.add(slo);
                    slo.setId(sloType.getSLOID());
                    slo.setMetricId(metric);
                    slo.setCapability(capability);
                    slo.setImportanceLevel(Slo.ImportanceLevel.valueOf(sloType.getImportanceWeight().value()));
                    if (sloType.getSLOexpression().getOneOpExpression() != null) {
                        SLOexpressionType.OneOpExpression expr = sloType.getSLOexpression().getOneOpExpression();
                        slo.setOperator(expr.getOperator().value());
                        slo.setValue(expr.getOperand().toString());
                    } else if (sloType.getSLOexpression().getTwoOpExpression() != null) {
                        SLOexpressionType.TwoOpExpression expr = sloType.getSLOexpression().getTwoOpExpression();
                        slo.setOperator(expr.getOperator().value());
                        slo.setValue1(expr.getOperand1().toString());
                        slo.setValue2(expr.getOperand2().toString());
                    } else {
                        throw new SupplyChainManagerException("Missing SLO expression for the SLO " + sloType.getSLOID());
                    }
                }
            }
        }

        LinkedHashMap<String, List<String>> mechanismsPerMetric = new LinkedHashMap<>();

        logger.debug("Metrics: {}", metrics);
        for (String metric : metrics) {
            List<String> metricMechanisms = retrieveSecurityMechanisms(metric);
            mechanismsPerMetric.put(metric, metricMechanisms);
        }

        // build sets of mechanisms
        // TODO: mock
        List<MechanismsSet> mechanismsSets = new ArrayList<>();
        MechanismsSet mechanismsSet1 = new MechanismsSet();
        mechanismsSets.add(mechanismsSet1);
        for (String metric : mechanismsPerMetric.keySet()) {
            List<String> mechanisms = mechanismsPerMetric.get(metric);
            if (mechanisms.isEmpty()) {
                throw new SupplyChainManagerException("No security mechanism found for the metric '" + metric + "'.");
            }
            String firstMechanism = mechanisms.get(0);
            mechanismsSet1.put(firstMechanism, metric);
        }

        // create supply chains
        List<SupplyChain> supplyChainsOverall = new ArrayList<>();

        // iterate over sets of  mechanisms
        for (MechanismsSet mechanismsSet : mechanismsSets) {
            // prepare input data for supply chain activity
            SupplyChainActivity scaData = new SupplyChainActivity();
            scaData.setSlaId(agreementOffer.getName());
            List<String> securityMechanisms = new ArrayList<>();
            securityMechanisms.addAll(mechanismsSet.getMechanisms());
            scaData.setSecurityMechanisms(securityMechanisms);
            scaData.setCloudResources(cloudResourcesList);
            scaData.setSlos(slos);

            // build supply chains
            List<SupplyChain> supplyChainsForMechanismSet = buildSupplyChains(scaData);
            supplyChainsOverall.addAll(supplyChainsForMechanismSet);
        }

        logger.debug("Supply chains created:\n{}", JsonDumper.dump(supplyChainsOverall));

        return supplyChainsOverall;
    }

    private List<String> retrieveSecurityMechanisms(String metricId) throws SupplyChainManagerException {
        // retrieve list of security mechanisms from the Service manager for the specified metric
        CollectionSchema securityMechanismsCollection = serviceManagerClient.getSecurityMechanisms(metricId);

        if (securityMechanismsCollection.getMembers() == 0) {
            throw new SupplyChainManagerException(String.format(
                    "No security mechanisms found for the metric %s.", metricId));
        }

        List<String> securityMechanisms = new ArrayList<>();

        // parse security mechanisms IDs
        Pattern smIdPattern = Pattern.compile("/([\\w-]+)$");
        for (String smUri : securityMechanismsCollection.getItemList()) {
            Matcher m = smIdPattern.matcher(smUri);
            if (m.find()) {
                securityMechanisms.add(m.group(1));
            } else {
                throw new SupplyChainManagerException("Invalid security mechanism URI: " + smUri);
            }
        }

        return securityMechanisms;
    }

    private List<SupplyChain> buildSupplyChains(SupplyChainActivity scaInput) throws SupplyChainManagerException {
        logger.debug("Creating supply chain activity at Planning...");
        if (logger.isTraceEnabled()) {
            logger.trace("Supply chain activity input data:\n{}", JsonDumper.dump(scaInput));
        }

        SupplyChainActivity sca = planningClient.createSupplyChainActivity(scaInput);

        logger.debug("Supply chain activity created successfully. ID: {}", sca.getId());

        // wait until the supply chain activity is finished
        Date startTime = new Date();
        do {
            logger.debug("Retrieving supply chain activity state...");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new SupplyChainManagerException("SupplyChainManager thread interrupted.");
            }

            SupplyChainActivity.Status status = planningClient.getSCAStatus(sca);
            logger.debug("Supply chain activity state: {}", status);

            if (status == SupplyChainActivity.Status.COMPLETED || status == SupplyChainActivity.Status.ERROR) {
                break;
            }
            if (new Date().getTime() - startTime.getTime() > 30000) {
                // timeout
                throw new SupplyChainManagerException("Timeout waiting for supply chain activity process to finish.");
            }
        } while (true);

        logger.debug("Retrieving list of built supply chains from the Planning for the supply chain activity {}...",
                sca.getId());

        List<SupplyChain> supplyChains = planningClient.getSupplyChains(sca);

        if (logger.isTraceEnabled()) {
            logger.trace("Supply chains returned by the Planning:\n{}", JsonDumper.dump(supplyChains));
        }
        return supplyChains;
    }

    private static class MechanismsSet {
        private Map<String, Set<String>> mechanismsData = new LinkedHashMap<>();

        public Map<String, Set<String>> getMechanismsData() {
            return mechanismsData;
        }

        public Set<String> getMechanisms() {
            return mechanismsData.keySet();
        }

        public void put(String mechanism, String metric) {
            if (!mechanismsData.containsKey(mechanism)) {
                mechanismsData.put(mechanism, new HashSet<String>());
            }
            mechanismsData.get(mechanism).add(metric);
        }
    }
}
