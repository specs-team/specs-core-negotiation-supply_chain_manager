package eu.specs.project.negotiation.scm.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.project.negotiation.scm.exceptions.SupplyChainManagerException;
import eu.specs.project.negotiation.scm.utils.CollectionSchema;
import eu.specs.project.negotiation.scm.utils.JsonDumper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class ServiceManagerClient {
    private static final Logger logger = LogManager.getLogger(ServiceManagerClient.class);
    private WebTarget serviceManagerTarget;

    public ServiceManagerClient(String serviceManagerApiAddress) {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        serviceManagerTarget = client.target(serviceManagerApiAddress);
    }

    public CollectionSchema getSecurityMechanisms(String metricId) throws SupplyChainManagerException {
        try {
            logger.debug("Retrieving list of security mechanisms for the metric '{}' from the Service Manager...",
                    metricId);

            CollectionSchema collection = serviceManagerTarget
                    .path("/security-mechanisms")
                    .queryParam("metric_id", metricId)
                    .request(MediaType.APPLICATION_JSON)
                    .get(CollectionSchema.class);

            if (logger.isDebugEnabled()) {
                logger.trace("Response:\n{}", JsonDumper.dump(collection));
            }

            return collection;

        } catch (Exception e) {
            throw new SupplyChainManagerException(String.format(
                    "Failed to retrieve security mechanisms for the metric %s: %s", metricId, e.getMessage()), e);
        }
    }
}
