package eu.specs.project.negotiation.scm;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.project.negotiation.scm.utils.CollectionSchema;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class ServiceManagerMock {

    private ObjectMapper objectMapper;

    public ServiceManagerMock() throws Exception {
        objectMapper = new ObjectMapper();
    }

    public void mockSecurityMechanisms() throws IOException {

        // security mechanism WebPool (SM1)
        CollectionSchema collectionSchema1 = new CollectionSchema();
        collectionSchema1.setMembers(1);
        collectionSchema1.setItemList(Arrays.asList(
                "http://localhost:8888/service-manager/cloud-sla/security-mechanisms/SM1"));

        stubFor(get(urlPathEqualTo("/service-manager/cloud-sla/security-mechanisms"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("metric_id", matching("level_of_redundancy_m1"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(collectionSchema1))));

        stubFor(get(urlPathEqualTo("/service-manager/cloud-sla/security-mechanisms"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("metric_id", matching("level_of_diversity_m2"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(collectionSchema1))));

        // security mechanism SVA (SM2)
        CollectionSchema collectionSchema2 = new CollectionSchema();
        collectionSchema2.setMembers(1);
        collectionSchema2.setItemList(Arrays.asList(
                "http://localhost:8888/service-manager/cloud-sla/security-mechanisms/SM2"));

        stubFor(get(urlPathEqualTo("/service-manager/cloud-sla/security-mechanisms"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("metric_id", matching("basic_scan_frequency_m13"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(collectionSchema2))));

        stubFor(get(urlPathEqualTo("/service-manager/cloud-sla/security-mechanisms"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("metric_id", matching("list_update_frequency_m14"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(collectionSchema2))));

        stubFor(get(urlPathEqualTo("/service-manager/cloud-sla/security-mechanisms"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("metric_id", matching("extended_scan_frequency_m22"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(collectionSchema2))));

        stubFor(get(urlPathEqualTo("/service-manager/cloud-sla/security-mechanisms"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("metric_id", matching("up_report_frequency_m23"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(collectionSchema2))));

        stubFor(get(urlPathEqualTo("/service-manager/cloud-sla/security-mechanisms"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("metric_id", matching("pen_testing_activated_m24"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(collectionSchema2))));

        CollectionSchema collectionSchema3 = new CollectionSchema();
        collectionSchema3.setMembers(0);
        collectionSchema3.setItemList(new ArrayList<String>());

        stubFor(get(urlPathEqualTo("/service-manager/cloud-sla/security-mechanisms"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("metric_id", matching("unsupported_metric"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(collectionSchema3))));
    }
}
