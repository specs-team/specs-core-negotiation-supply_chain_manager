package eu.specs.project.negotiation.scm;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.datamodel.enforcement.SupplyChainActivity;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class PlanningMock {
    private static String SCA_ID = "2d7f1189-0a53-4ed4-8c3d-a5518c6cb3c3";

    private ObjectMapper objectMapper;

    public PlanningMock() throws Exception {
        objectMapper = new ObjectMapper();
    }

    public void setUp(AgreementOffer agreementOffer) throws IOException, JSONException {

        SupplyChainActivity sca = new SupplyChainActivity();
        sca.setId(SCA_ID);

        stubFor(post(urlPathEqualTo("/planning-api/sc-activities"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(sca))));

        Map<String, String> stateResp = new HashMap<>();
        stateResp.put("state", SupplyChainActivity.Status.COMPLETED.name());

        stubFor(get(urlPathEqualTo(String.format("/planning-api/sc-activities/%s/state", sca.getId())))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(stateResp))));


        List<SupplyChain> supplyChains = objectMapper.readValue(
                this.getClass().getResourceAsStream("/supply-chains.json"), new TypeReference<List<SupplyChain>>() {
                });

        stubFor(get(urlPathEqualTo(String.format("/planning-api/sc-activities/%s/supply-chains", sca.getId())))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(supplyChains))));
    }
}
