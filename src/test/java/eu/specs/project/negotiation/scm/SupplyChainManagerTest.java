package eu.specs.project.negotiation.scm;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.project.negotiation.scm.exceptions.SupplyChainManagerException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.util.SocketUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SupplyChainManagerTest {
    private static final Logger logger = LogManager.getLogger(SupplyChainManagerTest.class);
    private final static String SERVICE_MANAGER_API_ADDRESS = "http://localhost:%d/service-manager/cloud-sla";
    private final static String PLANNING_API_ADDRESS = "http://localhost:%d/planning-api";
    private static int wiremockPort;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wiremockPort);

    @BeforeClass
    public static void setUpWiremock() {
        wiremockPort = SocketUtils.findAvailableTcpPort();
        logger.debug("Using port {} for WireMock server.", wiremockPort);
    }

    @Test
    public void testSupplyChainManager() throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller u = jaxbContext.createUnmarshaller();
        AgreementOffer agreementOffer = (AgreementOffer) u.unmarshal(
                this.getClass().getResourceAsStream("/SLATemplate-WebPool-SVA.xml"));

        new ServiceManagerMock().mockSecurityMechanisms();
        new PlanningMock().setUp(agreementOffer);

        SupplyChainManager supplyChainManager = new SupplyChainManager(
                String.format(SERVICE_MANAGER_API_ADDRESS, wiremockPort),
                String.format(PLANNING_API_ADDRESS, wiremockPort));
        List<SupplyChain> supplyChains = supplyChainManager.buildSupplyChains(agreementOffer);

        assertEquals(supplyChains.size(), 4);
    }

    @Test(expected = SupplyChainManagerException.class)
    public void testUnsupportedMetric() throws Exception {
        String content = new String(Files.readAllBytes(
                Paths.get(ClassLoader.getSystemResource("SLATemplate-WebPool-SVA.xml").toURI())));
        content = content.replace("level_of_redundancy_m1", "unsupported_metric");
        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller u = jaxbContext.createUnmarshaller();
        AgreementOffer agreementOffer = (AgreementOffer) u.unmarshal(new StringReader(content));

        new ServiceManagerMock().mockSecurityMechanisms();
        new PlanningMock().setUp(agreementOffer);

        SupplyChainManager supplyChainManager = new SupplyChainManager(
                String.format(SERVICE_MANAGER_API_ADDRESS, wiremockPort),
                String.format(PLANNING_API_ADDRESS, wiremockPort));
        supplyChainManager.buildSupplyChains(agreementOffer);
    }
}